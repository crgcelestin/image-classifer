# AI Programming with Python Project

## Summary
> ### Udacity AI with Python nanondegree final project
> - Project involves implementing an app that clasifies images
> - Application process encompossases the training of a deep learning machine model on a dataset of labeled flowers in order to recognize the species of newly inputted flower images


## Project Background
This is the Final Capstone for the Udacity Nano-Degree Program with the aim of the project being to develop and train a neural network based on Vgg16 architecture. 

Training was performed with a 102 Category Flower Dataset. Following training, my image classifier was assessed for accuracy using flower images not previously provided. 

The project is seperated into two parts: 

- Part 1 (took place in jupyter notebook)
Aims to prepare the tensor data and perform label mapping, upload the pretrained model and prepare the classifier

- Part 2 (fmodel.py, futility.py, predict.py, train.py) 
Involved refactoring code into helper functions and convert the Image Classification Project into CLI commands

## Files
- Image Classifier Project.html: CSS required for jupyter notebook

- Image Classifier Project.ipynb: Demonstrates all steps required to train VGG16 model using PyTorch to predict type of flower from its image.

- futility.py: First file created in refactoring process
    - Declares model to be utilized in training purposes
    - Primarily loads data set
    - For training, datasets have to have transformations applied to it such as random scaling, cropping, and flipping. Images also have to be normalized using means and standard deviations that the network expects. Paths were also defined for training purposes.

- fmodel.py: 
    - Performs network setup which involves defining network structure, dropout rate, as well as defining the device.
    - Building and training the network 
    - Saving and loading training checkpoint
    - Perform image processing and output predictions

- predict.py
    - imports futility, fmodel and carries out predictions
    - Python CLI app that utilizes the deep learning network and classifies an input flower image.
    - Opens the json file that enables proper label mapping
    - Then carries out prediction algorithm

- train.py
    - imports futility, fmodel and carries out training
    - Calculates accuracy of labels outputted from vgg16 model

## Technology Library
- Python
- PIL
- Numpy
- Torch
- Torchvision
- Matplotlib
- JSON
- VGG16
- Argparse 

## Project Goals
- Successfully build and train the model using epochs
- Test Network and Perform Classification with results greater than 70% accuracy

## Preparation of Tensor Data and Label Maps
- Torchvision was used to load data and the dataset was split into three parts for the training, validation, and testing processes
    - Training: Apply transformations in order for network to greater generalize for optimized performance
    - Validation and Testing: Measure the model's performance on data that is not previoulsy seen
    - All three sets require normalization of means and standard deviations of images to what is expected by the network
- Label Mapping
    - Load in mapping from category label to category name and read this in a 'cat_to_name.json' file 

## Build and Train Classifier
- Process involves using a pretrained model from torchvision to get image features and thus a feed-forward classifier can be built with loaded input
    - In this situation I use a network from VGG
    - A new untrained feed-forward network is defined as a classifier, ReLU is used for activations and dropout
    - Classifier layers are trained using backpropogation w/ pre-trained network to get features
    - Loss and accuracy are tracked on validation set to determine best hyperparmeters

## Network Testing
- Test images are ran through the network and accuracy is determined by how close predictions of flower category are to actual
- 70% accuracy is the baseline minimum on test set if model is trained well
import argparse
import json
import numpy as np
import matplotlib.pyplot as plt
import futility
import fmodel
import torch
from PIL import Image
from collections import OrderedDict
from torch import nn, optim
from torchvision import transforms, datasets, models
import torch.nn.functional as F
from torch.autograd import Variable

parser=argparse.ArgumentParser(
    description='Parser for train.py'
)
parser.add_argument(
    'data_dir',
    action='store',
    default='./flowers/'
)
parser.add_argument(
    '--save_dir',
    action='store',
    default='./checkpoint.pth',
)
parser.add_argument(
    '--arch',
    action='store',
    default='vgg16'
)
parser.add_argument(
    '--learning_rate',
    action='store',
    type=float,
    default=0.001
)
parser.add_argument(
    '--hidden_units',
    action='store',
    dest='hidden_units',
    type=int,
    default=512
)
parser.add_argument(
    '--epochs',
    action='store',
    default=3,
    type=int
)
parser.add_argument(
    '--dropout',
    action='store',
    type=float,
    default=0.2
)
parser.add_argument(
    '--gpu',
    action='store',
    default='gpu'
)



args=parser.parse_args()
where=args.data_dir
path=args.save_dir
lr=args.learning_rate
structure=args.arch
hidden_units=args.hidden_units
power=args.gpu
epochs=args.epochs
dropout=args.dropout

if torch.cuda.is_available() and power=="gpu":
    device=torch.device('cuda:0')
else:
    device=torch.device('cpu')

def main():
    trainloader, validloader, testloader, train_data = futility.load_data(where)
    model, criterion = fmodel.setup_network(structure, dropout, hidden_units, lr, power)
    optimizer=optim.Adam(model.classifier.parameters(),lr=0.001)
    steps=0
    running_loss=0
    print_every=5
    print('STARTING TRAINING')
    for epoch in range(epochs):
        for inputs, labels in trainloader:
            steps+=1
            if torch.cuda.is_available() and power=='gpu':
                inputs, labels=inputs.to(device), labels.to(device)
                model=model.to(device)
            optimizer.zero_grad()
            forwards=model.forward(inputs)
            loss=criterion(forwards, labels)
            loss.backward()
            optimizer.step()
            running_loss+=loss.item()
            if steps % print_every==0:
                valid_loss=0
                accuracy=0
                model.eval()
                with torch.no_grad():
                    for inputs, labels in validloader:
                        inputs, labels=inputs.to(device), labels.to(device)
                        forwards=model.forward(inputs)
                        labels_loss=criterion(forwards, labels)
                        valid_loss+=labels_loss.item()
                        ps=torch.exp(forwards)
                        #calc accuracy of labels
                        top_p, top_class=ps.topk(1, dim=1)
                        equals=top_class==labels.view(*top_class.shape)
                        accuracy+=torch.mean(equals.type(torch.FloatTensor)).item()

                print(f"Epoch {epoch+1}/{epochs}.. "
                    f"Loss: {running_loss/print_every:.3f}.. "
                    f"Validation Loss: {valid_loss/len(validloader):.3f}.. "
                    f"Accuracy: {accuracy/len(validloader):.3f}")
                running_loss=0
                model.train()
        model.class_to_idx=train_data.class_to_idx
        torch.save({
                'structure': structure,
                'hidden_units': hidden_units,
                'learning_rate': lr,
                'dropout': dropout,
                'no_of_epochs': epochs,
                'state_dict': model.state_dict(),
                'class_to_idx': model.class_to_idx}, path)
        print('checkpoint is saved!')
if __name__ =="__main__":
    main()
